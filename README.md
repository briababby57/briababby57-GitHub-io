********
# Code Guide
*******
<footer>
 <p>This is a code guide so that i can always come back to for help. </p>
 <p>This Serves as a cheatsheet for Markdown, Python, Javascript, CSS, & HTML.</p>
 </footer>

> Anything that is useful during my coding journey.

````python

prices = {
    'apple': 0.40, 'banana': 0.50
}
}
my_purchase = {
    'apple': 1,
    'banana': 6
}
grocery_bill = sum(prices[fruit] * my_purchase[fruit]
    for fruit in my_purchase)
print ('I owe the grocer $%.2f' % grocery_bill)
````
> [!NOTE]
> Highlights information that users should take into account, even when skimming.
 <details>
<summary>languages described</summary>

| Rank | Languages |
|-----:|-----------|
|     1| Pythom|
|     2| CSS    |
|     3| Javascript       |
|     4| HTML.    |

</details>

