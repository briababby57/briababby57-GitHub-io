# Exercise
1. Print your name using the print() function.
2. Create variables representing your age and height, and print them.
3. Do some basic arithmetic: add, subtract, multiply, and divide some numbers.
4. Try using the modulus and exponentiation operators.
5. Find out the types of all the variables you’ve created using the type() function.
