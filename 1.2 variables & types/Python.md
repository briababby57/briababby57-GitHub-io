# 1.2 Variables & Types & 1.3 Basic Arithmetic

## 1.2

You dont have ro declare a the type of variable Python will automatically. The most common types of data python uses are:

   1. int: an integer, e.g., 1, 2, 3
   2. float: a floating-point (decimal) number, e.g., 1.0, 1.5, 3.14
   3. str: a string (text), e.g., “hello”, “world”
   4. bool: a boolean, which can be True or False.
   5. To check the type of variable use the type() function:

   ````python
   print(type(name)) # <class 'str'>
   ````

## 1.3 Basic Arithmetic

   Heres the basic arithmetics python accepts:
   - Addition (+)

- Subtraction (-)
- Multiplication (*)
- Division (/)
- Exponentiation (**)
- Modulus (%)
- Floor division (//)
Heres how they can be used:

````python
x = 10
y = 2

print(x + y) # 12
print(x - y) # 8
print(x * y) # 20
print(x / y) # 5.0
print(x ** y) # 100
print(x % y) # 0
print(x // y) # 5
````
